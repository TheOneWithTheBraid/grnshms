# GRNSMS - Xitlab Xunner Xix Xhell Xacos Xandbox

> GitLab Runner nix-shell macOS sandbox

Run GitLab Runner jobs in a macOS application sandbox with nix-shell.

## What the f\*\*\* ?

Started as a private playground, I'm now trying to get this more production ready.

State of the project : Rather don't take it serious - aka ready for professional use.

## Setup

### Prepare nix

Please be careful. The nixos official nix installer does not survive system updates on macOS.

Please carefully follow the instructions
of [Determinate Systems](https://github.com/DeterminateSystems/nix-installer#readme).

```shell
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update
```

### Store Xcode in nix-cache

Please
check [the Xcode nixpkg](https://github.com/NixOS/nixpkgs/blob/master/pkgs/os-specific/darwin/xcode/default.nix#L57) for
the latest version supported.

Xcode sadly doesn't allow automated downloads. Please manually download Xcode as a XIP file
from https://download.developer.apple.com/ .

For legal reasons I won't document how to do this from CLI. Best practice would be downloading it on the machine /
server deploying the Ansible playbook and uploading it during setup via SCP to the macOS machine.

Now extract the XIP file using nix-shell.
(avoids need to use `open` with running desktop environment required) - there's nothing you can't solve using a tiny
nix-shell *hihi*

Source : https://github.com/NixOS/nixpkgs/blob/master/pkgs/os-specific/darwin/xcode/default.nix#L12

```shell
export XCODE_FILE=Xcode_*.xip

# will take long and won't show and output for pretty long. This is normal. You didn't make the mac explode (yet)
nix-shell -p xar pbzx rcodesign cpio --run "xar -xf $XCODE_FILE && rm -rf $XCODE_FILE && sh -c 'pbzx -n Content | cpio -i' && rm Content Metadata && rcodesign verify Xcode.app/Contents/MacOS/Xcode"

nix-store --add-fixed --recursive sha256 Xcode.app
mv Xcode.app /Applications
```

### Increase the ulimit of your macOS installation

Yes, macOS was never intended to be a server OS.

```shell
echo 'kern.maxfiles=1048576' | sudo tee -a /etc/sysctl.conf
echo -e 'limit maxfiles 32768 1048576\nlimit maxproc 2000 8000' | sudo tee -a /etc/launchd.conf
echo 'ulimit -n 32768' | sudo tee -a /etc/profile
```

### Install GitLab runner

You should install the `gitlab-runner` executable as a non-administrator user.

##### Requisites

- Ensure your service user is configured
  to [log in automatically during system startup](https://support.apple.com/en-us/102316)
- Ensure you [increased the ulimit of your machine](#increase-the-ulimit-of-your-macOS-installation)

Logged in to your service user, execute the following commands :

```shell
nix-channel --update
nix-env --install gitlab-runner
gitlab-runner install
```

***Please ignore the warning suggesting to install the GitLab runner as root. This is not supported on macOS.

### Configure your runner

For further macOS related gitlab-runner troubleshooting, please
consult : https://docs.gitlab.com/runner/install/osx.html#known-issues .

Please ensure your runner is *not* running as root. If you have a system-wide installation, consider the following
workarounds :

<details>

<summary>Configure the LaunchAgent of your installation</summary>

##### gitlab-runner.plist

Please populate the following key in `~/Library/LaunchAgents/gitlab-runner.plist` in order to support for code signing
during CI jobs :

```plist
        <key>SessionCreate</key>
	    <true/>
```

Please additionally check that `--working directory` and `--config-path` are set correctly.

##### OSX Keychain issues with git

Moreover, due to an [issue in gitlab-runner upstream](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/36952), you
need to ensure the OSX Keychain Git credential helper can't run - yes, this is an ugly workaround.

Please add the following to the Crontab of your `GITLAB_USER` (`crontab -u GITLAB_USER -e`):

```cron
* * * * * killall git-credential-osxkeychain
```

</details>

### Configure the GitLab Runner to use grnshms as custom executor

```toml
[[runners]]
name = "nix-shell-sandbox"
executor = "custom"
# the global location of the build home
builds_dir = "/private/var/tmp/grnshms_home/build"
cache_dir = "/private/var/tmp/grnshms_home/cache"
    [runners.custom]
    prepare_exec = "/bin/bash"
    run_exec = "/path/to/grnshms/runner.sh"
```

### Install sudoers file

In pretty most cases, you will need access to `xcode-select` and `xcodebuild` in your jobs. This once requires root.

```shell
# verify the contents of the sudoers file
sudo install -oroot -gwheel -m440 50-sudo-xcode /etc/sudoers.d
```

### Configure your nix-shell input

```nix
with (import <nixpkgs> {});

let
  inputs = [
    cacert
    git
    coreutils
    colima
    docker
    ideviceinstaller
    # darwin.xcode_15_1
    ruby
    cocoapods
    python3
    python3Packages.virtualenv
    xcpretty
    ];
in mkShell {
  buildInputs = inputs;
  shellHook =
  ''
    # ugly workaround to prevent use of nix-provided clang
    mkdir -p "$HOME/.bin"
    ln -sf /usr/bin/clang "$HOME/.bin"
    ln -sf /usr/bin/clang++ "$HOME/.bin"
    ln -sf /usr/bin/sed "$HOME/.bin"
    ln -sf /usr/bin/tar "$HOME/.bin"

    # ensure we use the system xcrun
    ln -sf /usr/bin/xcrun "$HOME/.bin"

    export LANG=en_US.UTF-8

    # create a clean build environment for our Python toolchain
    rm -rf .buildenv
    python -m virtualenv .buildenv

    # either specify a per-job Xcode version

    # export XCODE_HASH="hvqfks6vchhg3pzszqs064hy27cxws3q"
    # export XCODE_APP="/nix/store/$XCODE_HASH-Xcode.app"

    # or use the system Xcode version

    export XCODE_APP="/Applications/Xcode.app"

    # these environment variables override the xcode-select location for some tools, manually overriding
    export DEVELOPER_DIR="$XCODE_APP/Contents/Developer"
    export SDKROOT="$DEVELOPER_DIR/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk"

    sudo xcode-select -s "$DEVELOPER_DIR"
    sudo xcodebuild -license accept
    sudo xcodebuild -runFirstLaunch
    sudo /usr/sbin/softwareupdate --install-rosetta --agree-to-license

    xcodebuild -quiet -downloadPlatform iOS

    # ensure we have the system linker and compile in first position of PATH
    export LD="/usr/bin/clang"

    export PATH="$HOME/.bin:$GEM_HOME/bin:$DEVELOPER_DIR/usr/bin:$DEVELOPER_DIR/Toolchains/XcodeDefault.xctoolchain/usr/bin:$FLUTTER_HOME/bin:$PATH:/usr/sbin:/usr/bin"

    source .buildenv/bin/activate
    pip install codemagic-cli-tools

    gem install --update xcodeproj
    pod repo update

  '';
}
```

## Credits

uwu Meow <3 uwu

> Where Awoo ?

Awoo !

Additionally credits to :

- https://github.com/fkorotkov for some sample macOS sandbox file.
- [x-tention GmbH](https://x-tention.at/) for the debugging time

## License

This project is licensed under the terms and conditions of the [EUPL 1.2](LICENSE).
