#!/usr/bin/env bash

set -e

if [ "$(uname -o)" != "Darwin" ]; then
  echo "This piece of software can *really* only run on Darwin."
  exit 42
fi

readonly SCRIPT_FILE="${@: -2}"

export PATH="$PATH:/nix/var/nix/profiles/default/bin"

# create a build home for dependencies
export HOME="/private/var/tmp/grnshms_home"
mkdir -p "${HOME}"
export XDG_CONFIG_HOME="$(mktemp -d)/.config"
mkdir -p "${XDG_CONFIG_HOME}"

export LANG=en_US.UTF-8

# prepare nix-env in build home
if [ "$(nix-channel --list | grep -c nixpkgs)" -eq 0 ]; then
	nix-channel --add https://nixos.org/channels/nixpkgs-unstable
	mkdir -p ~/.config/nixpkgs
	echo "{ allowUnfree = true; }" > ~/.config/nixpkgs/config.nix
fi

readonly BASE_PACKAGES="darwin.shell_cmds nix git wget curl gitlab-runner"
readonly NIX_PATH=$(nix-shell -p ${BASE_PACKAGES} --pure --run "echo \$PATH")
readonly SANDBOX_LOCATION=$(dirname "$0")

if [[ "$2" == "prepare_script" ]]; then
	nix-channel --update
fi

if [[ "$2" == "step_script" || "$2" == "build_script" || "$2" == "after_script" ]]; then
  readonly ENTRYPOINT=""
else
  readonly ENTRYPOINT="-p ${BASE_PACKAGES}"
fi

mkdir -p "${CUSTOM_ENV_CI_PROJECT_DIR}"
cd "${CUSTOM_ENV_CI_PROJECT_DIR}"

PATH=/bin:/usr/bin:/usr/local/bin:$NIX_PATH sandbox-exec -f "${SANDBOX_LOCATION}/runner.sb" nix-shell ${ENTRYPOINT} --command "bash ${SCRIPT_FILE}"
