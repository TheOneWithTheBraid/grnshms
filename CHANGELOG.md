# v0.2.0

- fix: weaken sandbox permissions (The one with the braid)

# v0.1.1

- chore: add further setup instructions (The one with the braid)
- fix: add gitlab-runner to base packages (The one with the braid)
- fix: add missing permissions to readme (The one with the braid)

# v0.1.0

- fix: add /tmp to sandbox (The one with the braid)
- fix: add Xcode cache to sandbox (The one with the braid)
- fix: set `XDG_CONFIG_HOME` (The one with the braid)
